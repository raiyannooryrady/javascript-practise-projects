let x=10;
if(true){
    let x=5;
    var y=4;//block e var deua ar global e var deua same
    console.log('access let declared variable x from block scope'+x);
    console.log('access var declared variable y from block scope'+y);
    var y=7;
    console.log('access changed var declared variable y from block scope'+y);
    //let x=6; cannot redeclare let
}
//console.log(x); cannot access from here because it's under a block scope
console.log('access var declared variable y from global scope'+y);
console.log('access globally let defined variable x from global scope'+x);

//var loveName
//loveName=undefined
//var hoisted hoise
loveName="beloved";
var loveName;
console.log(loveName);//declared later also work for var

//homeName="acces not allowed";
//undefine assign kore na let er khetre
//let o hoisted hoy
let homeName;
homeName="acces allowed";
console.log(homeName);

let bookName;
bookName='story';
console.log(bookName);